/*
  Arrow functions + default values
  Docs: https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Functions/Arrow_functions
*/

    var x = ( param ) => { console.log( param ); };

    var y = function( param ){
      console.log( param );
    };

    // x("Arrow Fuction");

    var Duble = a => a * 2;
    var Duble4 = Duble(4);
    // console.log(Duble4);
